<?php

	function newHeader($name, $source){
		echo('<br><h1 style="margin:0">' . $name . '</h1><p style="margin: 5px;">(<a href="https://github.com/NerdsWBNerds/' . $source . '">Source</a>)</p>');
	}
	
	function newWarning($warning){
		echo('<p class="warning">' . $warning . '</p>');
	}
	
	function newSWarn(){
		newWarning("WARNING! Flashing colors might induce seizures.");
	}
	
	function newInfo($i){
		echo('<p class="info">' . $i . '</p>');
	}
	
?>